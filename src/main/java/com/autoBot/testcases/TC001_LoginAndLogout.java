package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.testleaf.taps.LoginPage;
// Annotations == ProjectBase
public class TC001_LoginAndLogout extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login into leaftaps";
		author = "koushik";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String un , String pwd) {
		
		new LoginPage()
		.enterUserName(un)
		.enterPassword(pwd)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickCreate();
//		clearAndType(locateElement("id", "username"),data[0]);
//		clearAndType(locateElement("id", "password"),data[1]);
//		click(locateElement("class", "decorativeSubmit"));
		
	}
	
}






