package com.testleaf.taps;

import com.autoBot.testng.api.base.Annotations;

public class CreateLead extends Annotations{
	public CreateLead() {}
	public CreateLead enterComName() {
		clearAndType(locateElement("id","createLeadForm_companyName"),"Amazon");
		return this;
	}
	public CreateLead enterFName() {
		clearAndType(locateElement("id","createLeadForm_firstName"),"Narez");
		return this;
		
		
	}
	public CreateLead enterLName() {
		clearAndType(locateElement("id","createLeadForm_lastName"),"krish");
		return this;
	}
	public CreateLead clickCreateLead() {
		click(locateElement("class","smallSubmit"));
		System.out.println("Lead Created");
		return this;

}
}