package com.testleaf.taps;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations{
	public LoginPage() {
		PageFactory.initElements(driver, this);
		
		
	}
	
	@CacheLookup
	
	@FindBy(how=How.ID,using ="username")
	private WebElement eleusername;
	@FindBy(how=How.ID, using="password") 
	WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") 
	WebElement eleLogin;
	
	
public LoginPage enterUserName(String un) {
	clearAndType(eleusername,un);
	return this;
}
public LoginPage enterPassword(String str) {
	clearAndType(elePassWord,str);
	return this;
}
public HomePage clickLogin() {
	click(eleLogin);
	return new HomePage();
	
}
}





