package com.testleaf.taps;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadPage extends Annotations{
	public MyLeadPage() {}
	public CreateLead clickCreate() {
		click(locateElement("link","Create Lead"));
		return new CreateLead();
	}
}
