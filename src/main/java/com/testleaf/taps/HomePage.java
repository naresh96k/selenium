package com.testleaf.taps;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{
	public HomePage() {}
	public MyHomePage clickcrmsfa() {
		click(locateElement("link","CRM/SFA"));
		return new MyHomePage();
	}
}
